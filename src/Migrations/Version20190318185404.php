<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190318185404 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE arbitre (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(15) NOT NULL, nationalite VARCHAR(20) NOT NULL, prenom VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE arbitre_match (id INT AUTO_INCREMENT NOT NULL, arbitre_id INT NOT NULL, rencontre_id INT NOT NULL, INDEX IDX_EA3FB53D943A5F0 (arbitre_id), INDEX IDX_EA3FB53D6CFC0818 (rencontre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE but (id INT AUTO_INCREMENT NOT NULL, joueur_id INT NOT NULL, temps INT NOT NULL, INDEX IDX_B132FECAA9E2D76C (joueur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE but_adverse (id INT AUTO_INCREMENT NOT NULL, rencontre_id INT NOT NULL, temps INT NOT NULL, INDEX IDX_F867B8E56CFC0818 (rencontre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE changement (id INT AUTO_INCREMENT NOT NULL, entree_joueur_id INT DEFAULT NULL, sortie_joueur_id INT NOT NULL, rencontre_id INT NOT NULL, temps INT NOT NULL, temps_de_sortie INT DEFAULT NULL, INDEX IDX_964C71E2BF038CA6 (entree_joueur_id), INDEX IDX_964C71E2EE63D9DE (sortie_joueur_id), INDEX IDX_964C71E26CFC0818 (rencontre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faute (id INT AUTO_INCREMENT NOT NULL, arbitre_id INT NOT NULL, rencontre_id INT DEFAULT NULL, sanction INT NOT NULL, carton VARCHAR(255) NOT NULL, temps INT NOT NULL, INDEX IDX_777B6608943A5F0 (arbitre_id), INDEX IDX_777B66086CFC0818 (rencontre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faute_player (faute_id INT NOT NULL, player_id INT NOT NULL, INDEX IDX_F482763D9C22F06A (faute_id), INDEX IDX_F482763D99E6F5DF (player_id), PRIMARY KEY(faute_id, player_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE joueur_match (id INT AUTO_INCREMENT NOT NULL, joueur_id INT NOT NULL, rencontre_id INT NOT NULL, numero INT NOT NULL, position INT NOT NULL, poste INT NOT NULL, est_remplacant TINYINT(1) NOT NULL, INDEX IDX_A2EE7C03A9E2D76C (joueur_id), INDEX IDX_A2EE7C036CFC0818 (rencontre_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE player (id INT AUTO_INCREMENT NOT NULL, prenom VARCHAR(15) NOT NULL, nom VARCHAR(15) NOT NULL, est_titulaire INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rencontre (id INT AUTO_INCREMENT NOT NULL, equipe_adverse VARCHAR(20) NOT NULL, lieu VARCHAR(100) NOT NULL, date DATETIME NOT NULL, type_maillot INT NOT NULL, premiere_mi_temps TIME DEFAULT NULL, seconde_mi_temps INT DEFAULT NULL, capitaine INT DEFAULT NULL, suppleant INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rencontre_arbitre (rencontre_id INT NOT NULL, arbitre_id INT NOT NULL, INDEX IDX_1605D0C46CFC0818 (rencontre_id), INDEX IDX_1605D0C4943A5F0 (arbitre_id), PRIMARY KEY(rencontre_id, arbitre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE arbitre_match ADD CONSTRAINT FK_EA3FB53D943A5F0 FOREIGN KEY (arbitre_id) REFERENCES arbitre (id)');
        $this->addSql('ALTER TABLE arbitre_match ADD CONSTRAINT FK_EA3FB53D6CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id)');
        $this->addSql('ALTER TABLE but ADD CONSTRAINT FK_B132FECAA9E2D76C FOREIGN KEY (joueur_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE but_adverse ADD CONSTRAINT FK_F867B8E56CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id)');
        $this->addSql('ALTER TABLE changement ADD CONSTRAINT FK_964C71E2BF038CA6 FOREIGN KEY (entree_joueur_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE changement ADD CONSTRAINT FK_964C71E2EE63D9DE FOREIGN KEY (sortie_joueur_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE changement ADD CONSTRAINT FK_964C71E26CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id)');
        $this->addSql('ALTER TABLE faute ADD CONSTRAINT FK_777B6608943A5F0 FOREIGN KEY (arbitre_id) REFERENCES arbitre (id)');
        $this->addSql('ALTER TABLE faute ADD CONSTRAINT FK_777B66086CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id)');
        $this->addSql('ALTER TABLE faute_player ADD CONSTRAINT FK_F482763D9C22F06A FOREIGN KEY (faute_id) REFERENCES faute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE faute_player ADD CONSTRAINT FK_F482763D99E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE joueur_match ADD CONSTRAINT FK_A2EE7C03A9E2D76C FOREIGN KEY (joueur_id) REFERENCES player (id)');
        $this->addSql('ALTER TABLE joueur_match ADD CONSTRAINT FK_A2EE7C036CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id)');
        $this->addSql('ALTER TABLE rencontre_arbitre ADD CONSTRAINT FK_1605D0C46CFC0818 FOREIGN KEY (rencontre_id) REFERENCES rencontre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rencontre_arbitre ADD CONSTRAINT FK_1605D0C4943A5F0 FOREIGN KEY (arbitre_id) REFERENCES arbitre (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE arbitre_match DROP FOREIGN KEY FK_EA3FB53D943A5F0');
        $this->addSql('ALTER TABLE faute DROP FOREIGN KEY FK_777B6608943A5F0');
        $this->addSql('ALTER TABLE rencontre_arbitre DROP FOREIGN KEY FK_1605D0C4943A5F0');
        $this->addSql('ALTER TABLE faute_player DROP FOREIGN KEY FK_F482763D9C22F06A');
        $this->addSql('ALTER TABLE but DROP FOREIGN KEY FK_B132FECAA9E2D76C');
        $this->addSql('ALTER TABLE changement DROP FOREIGN KEY FK_964C71E2BF038CA6');
        $this->addSql('ALTER TABLE changement DROP FOREIGN KEY FK_964C71E2EE63D9DE');
        $this->addSql('ALTER TABLE faute_player DROP FOREIGN KEY FK_F482763D99E6F5DF');
        $this->addSql('ALTER TABLE joueur_match DROP FOREIGN KEY FK_A2EE7C03A9E2D76C');
        $this->addSql('ALTER TABLE arbitre_match DROP FOREIGN KEY FK_EA3FB53D6CFC0818');
        $this->addSql('ALTER TABLE but_adverse DROP FOREIGN KEY FK_F867B8E56CFC0818');
        $this->addSql('ALTER TABLE changement DROP FOREIGN KEY FK_964C71E26CFC0818');
        $this->addSql('ALTER TABLE faute DROP FOREIGN KEY FK_777B66086CFC0818');
        $this->addSql('ALTER TABLE joueur_match DROP FOREIGN KEY FK_A2EE7C036CFC0818');
        $this->addSql('ALTER TABLE rencontre_arbitre DROP FOREIGN KEY FK_1605D0C46CFC0818');
        $this->addSql('DROP TABLE arbitre');
        $this->addSql('DROP TABLE arbitre_match');
        $this->addSql('DROP TABLE but');
        $this->addSql('DROP TABLE but_adverse');
        $this->addSql('DROP TABLE changement');
        $this->addSql('DROP TABLE faute');
        $this->addSql('DROP TABLE faute_player');
        $this->addSql('DROP TABLE joueur_match');
        $this->addSql('DROP TABLE player');
        $this->addSql('DROP TABLE rencontre');
        $this->addSql('DROP TABLE rencontre_arbitre');
    }
}
