<?php

namespace App\Form;

use App\Entity\Joueur;
use App\Entity\Poste;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JoueurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prenom', null, [
                'label' => "Prénom"
            ])
            ->add('nom', null, [
                'label' => "Nom"
            ])
            ->add('postePredilection', EntityType::class, [
                'label' => "Poste de prédilection",
                'class' => Poste::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Joueur::class,
        ]);
    }
}
