<?php

namespace App\Form;

use App\Entity\JoueurRencontre;
use App\Entity\Joueur;
use App\Entity\Position;
use App\Entity\Poste;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class JoueurMatchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $choices = [
            'Membre d\'équipe' => "0",
            'Capitaine' => '1',
            'Suppléant' => '2'
        ];

        $builder
            ->add('numero', null, [
                'label' => "Numéro",
            ])
            ->add('position', EntityType::class, [
                'label' => "Position",
                'class' => Position::class,
                'empty_data' => 0,
                'required' => false,
            ])
            ->add('poste', EntityType::class, [
                'label' => "Poste",
                'class' => Poste::class
            ])
            ->add('estRemplacant', CheckboxType::class, [
                'data' => false,
                'label' => "Remplaçant"
            ])
            ->add('joueur', EntityType::class, [
                'label' => "Joueur",
                'class' => Joueur::class,
                'data' => 0
            ])
            ->add('status', ChoiceType::class, [
                'label' => "Status",
                'data' => 0,
                'expanded' => false,
                'choices' => $choices,
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JoueurRencontre::class,
        ]);
    }
}
