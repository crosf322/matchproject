<?php

namespace App\Form;

use App\Entity\But;
use App\Entity\JoueurRencontre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ButType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('joueur', EntityType::class, [
                'mapped' => false,
                'label' => "Joueur",
                'class' => JoueurRencontre::class,
                'choices' => $options['joueurs'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => But::class,
            'joueurs' => null
        ]);
    }
}
