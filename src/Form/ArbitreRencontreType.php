<?php

namespace App\Form;

use App\Entity\Arbitre;
use App\Entity\ArbitreRencontre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArbitreRencontreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('estPrincipal', null, [
                'label' => "Principal"
            ])
            ->add('arbitre', EntityType::class, [
                'label' => "Arbitre",
                'class' => Arbitre::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ArbitreRencontre::class,
        ]);
    }
}
