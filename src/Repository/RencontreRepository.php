<?php

namespace App\Repository;

use App\Entity\Rencontre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Rencontre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rencontre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rencontre[]    findAll()
 * @method Rencontre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RencontreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Rencontre::class);
    }


    public function findNext() {
        date_default_timezone_set('Europe/Paris');
        $now = new \DateTime();

        return $this->createQueryBuilder('m')
            ->where('m.date > :now')
            ->setParameter('now', $now)
            ->orderBy('m.date')
            ->getQuery()->getResult();
    }


    /**
     * @return Rencontre[] Returns an array of Rencontre objects
     * @throws \Doctrine\DBAL\DBALException
     */

    public function findBestComboByPostePosition(array $poste, array $position)
    {
        $combinaisonDT = [];

        //combinaison poste/position des 4 joueurs de la combinaison
        foreach ($poste as $key => $value){
            array_push($combinaisonDT,$poste[$key]);
            array_push($combinaisonDT,$position[$key]);
        }

        $liste = implode(',',$combinaisonDT);

        $rawSql = "
            CALL dreamteam($liste)
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );

        return $stmt->fetch();
    }

    public function getAllScore()
    {
        $rawSql = "
            CALL getScoreMatch;
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );

        return $stmt->fetchAll();
    }

    public function getAllEventsByMatch(int $matchId, JoueurRepository $joueurRepository)
    {
        $joueurs = $joueurRepository->findAll();
        $joueursIdIndex = [];

        $joueursIdIndex[0] = null;

        foreach ($joueurs as $joueur){
            $joueursIdIndex[$joueur->getId()] = $joueur;
        }

        $rawSql = "
            CALL events_by_match($matchId);
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );
        $results = $stmt->fetchAll();

      foreach ($results as $result){
          $j1 = null==$result['arg1']?0:(int)$result['arg1'];
          $j2 = null==$result['arg2']?0:(int)$result['arg2'];

          $joueur1 = $joueursIdIndex[$j1];
          $joueur2 = $joueursIdIndex[$j2];

            $return[] = [
                'evenement' => $result['evenement'],
                'arg1' => $joueur1,
                'arg2' => $joueur2,
                'arg3' => $result['arg3'],
                'temps' => $result['temps'],
            ];
      }
        return $return;
    }

//
//    public function getWinMatch()
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

}
