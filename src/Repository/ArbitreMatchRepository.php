<?php

namespace App\Repository;

use App\Entity\ArbitreRencontre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArbitreRencontre|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArbitreRencontre|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArbitreRencontre[]    findAll()
 * @method ArbitreRencontre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArbitreMatchRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArbitreRencontre::class);
    }

    public function findByMatch($rencontre)
    {
        return $this->createQueryBuilder('a')
            ->where('a.rencontre = :rencontre')
            ->join('a.arbitre', 'arbitre')
            ->addSelect('arbitre')
            ->setParameter('rencontre', $rencontre)
            ->getQuery()->getScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?ArbitreRencontre
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
