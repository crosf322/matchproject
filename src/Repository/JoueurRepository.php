<?php

namespace App\Repository;

use App\Entity\Joueur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Joueur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Joueur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Joueur[]    findAll()
 * @method Joueur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoueurRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Joueur::class);
    }



    public function tempTotalJoueur(Joueur $joueur, array $participations)
    {
        $joueurId= $joueur->getId();
        $tempsTotal = 0;

        foreach ($participations as $participation){
            $rawSql = "CALL temps_match_joueur($participation,$joueurId);";
            $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
            $stmt->execute();
            $result = $stmt->fetch()["temps_sur_terrain"];
            $tempsTotal += $result;
        }

        return $tempsTotal;
    }

    public function getListParticipation(Joueur $joueur)
    {
        $id = $joueur->getId();

        $rawSql = "
        	SELECT DISTINCT jr.rencontre_id
            FROM joueur_rencontre jr
            WHERE jr.joueur_id = $id;
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );
        $results = $stmt->fetchAll();
        $listeMatchJoue = [];

        foreach ($results as $result){
            $listeMatchJoue[] = $result["rencontre_id"];
        }

        return $listeMatchJoue;
    }
}
