<?php

namespace App\Repository;

use App\Entity\But;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method But|null find($id, $lockMode = null, $lockVersion = null)
 * @method But|null findOneBy(array $criteria, array $orderBy = null)
 * @method But[]    findAll()
 * @method But[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ButRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, But::class);
    }

//    public function findSumByMatch()
//    {
//        return $this->createQueryBuilder('b')
//            ->select('COUNT(b), r')
//            ->innerJoin('rencontre','r','b.rencontre=r')
//            ->groupBy('b.rencontre')
////            ->orderBy('b.rencontre', 'ASC')
//            ->getQuery()
//            ->getResult()
////            ->getScalarResult()
//            ;
//    }

    public function findByMatchId($idMatch)
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b)')
            ->andWhere('b.rencontre = :idMatch')
            ->setParameter('idMatch', $idMatch)
            ->getQuery()
            ->getResult()
            ;
    }


    // /**
    //  * @return But[] Returns an array of But objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?But
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
