<?php

namespace App\Repository;

use App\Entity\Changement;
use App\Entity\Joueur;
use App\Entity\JoueurRencontre;
use App\Entity\Rencontre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use phpDocumentor\Reflection\Types\Boolean;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Changement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Changement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Changement[]    findAll()
 * @method Changement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChangementRepository extends ServiceEntityRepository
{
    const TEMPS_MATCH = 90;

    public function __construct (RegistryInterface $registry)
    {
        parent::__construct( $registry, Changement::class );
    }

    /**
     * @return Changement[] Returns an array of Changement objects
     */
    public function findTempsByMatchByPlayer (Rencontre $match, Joueur $joueur)
    {
        $idMatch = $match->getId();
        $idJoueur = $joueur->getId();

        $entree = $this->createQueryBuilder( 'c' )
            ->andWhere( 'c.rencontre = :match' )
            ->setParameter( 'match', $match )
            ->andWhere( 'c.entreeJoueur = :joueur' )
            ->setParameter( 'joueur', $joueur )
            ->orderBy( 'c.id', 'ASC' )
            ->getQuery()
            ->getResult();

        $sortie = $this->createQueryBuilder( 'c' )
            ->andWhere( 'c.rencontre = :match' )
            ->setParameter( 'match', $match )
            ->andWhere( 'c.sortieJoueur = :joueur' )
            ->setParameter( 'joueur', $joueur )
            ->orderBy( 'c.id', 'ASC' )
            ->getQuery()
            ->getResult();


        $rawSql = "
            SELECT jr.est_remplacant
            FROM joueur_rencontre jr
            WHERE jr.rencontre_id = $idMatch AND jr.joueur_id = $idJoueur
        ";
        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );

        $aCommenceRemplacant = $stmt->fetch()['est_remplacant'];

        /** @var Changement $lastSortie */
        $lastSortie = array_pop( $sortie );
        /** @var Changement $lastEntree */
        $lastEntree = array_pop( $entree );


        if (!$aCommenceRemplacant) {
            if (null == $lastSortie) {
                return $match->getFinMatch();
            }
        } else {
            if (null == $lastEntree) {
                return 0;
            }

            if (isset( $entree[0] )) {
                $tempsInitial = $entree[0]->getTemps();
            } else {
                $tempsInitial = $lastEntree->getTemps();
            }
        }
        $tempsDeMatch = 0;

        while (isset( $lastSortie )) {
            null == $lastEntree ? $tempsEntree = 0 : $tempsEntree = $lastEntree->getTemps();

            if ($lastSortie->getTemps() > $tempsEntree) {
                if (null !== $lastSortie->getTempsDeSortie()) {
                    $tempsDeMatch -= ($lastSortie->getTempsDeSortie() + $lastSortie->getTemps());
                } else {
                    $tempsDeMatch += $lastSortie->getTemps();
                }

                $curseurMatch = $lastSortie->getTemps();
                $lastSortie = array_pop( $sortie );
            } else {
                if (!isset( $curseurMatch )) {
                    $curseurMatch = $lastEntree->getTemps();
                }

                if (!$aCommenceRemplacant) {
                    $tempsDeMatch += ($curseurMatch - $lastEntree->getTemps());
                } else {
                    $tempsDeMatch -= $lastEntree->getTemps();
                }

                $curseurMatch = $lastEntree->getTemps();
                $lastEntree = array_pop( $entree );

            }
        }
        if (isset( $tempsInitial )) {
            $tempsDeMatch -= $tempsInitial;
            unset( $tempsInitial );
        }
    }

    public function tempsMatchJoueur(Rencontre $match, Joueur $joueur)
    {
        $matchId = $match->getId();
        $joueurId = $joueur->getId();

        $rawSql = "
            CALL temps_match_joueur($matchId, $joueurId)
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );
        $result = $stmt->fetch();
        return $result['temps_sur_terrain'];
    }

    public function tempsMoyenJoueur (Joueur $joueur)
    {
        $joueurId = $joueur->getId();

        $rawSql = "
            CALL temps_moyen_joueur($joueurId)
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare( $rawSql );
        $stmt->execute( [] );
        $result = $stmt->fetch();
        return $result['temps_sur_terrain'];
    }

    public function tempsMoyenEquipe (JoueurRepository $joueurRepo)
    {
        $liste_joueur = $joueurRepo->findAll();

        $moyene_total_minutes=0;

        foreach ($liste_joueur as $key => $joueur) {
            $moyene_total_minutes += $this->tempsMoyenJoueur($joueur);
        }

        $moyene_total_minutes /= sizeof($liste_joueur);
        $moyene_total_percent = $moyene_total_minutes  * 100 / self::TEMPS_MATCH;

        return $moyene_total_percent;

    }
}
