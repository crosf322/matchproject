<?php

namespace App\Repository;

use App\Entity\JoueurRencontre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method JoueurRencontre|null find($id, $lockMode = null, $lockVersion = null)
 * @method JoueurRencontre|null findOneBy(array $criteria, array $orderBy = null)
 * @method JoueurRencontre[]    findAll()
 * @method JoueurRencontre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoueurMatchRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, JoueurRencontre::class);
    }


//    public function findByMatch($id)
//    {
//        $rawSql = "
//            SELECT j.prenom, j.nom, jm.numero, jm.poste, jm.POSITION, jm.est_remplacant
//            FROM joueur j
//            JOIN joueur_rencontre jm ON jm.joueur_id = j.id
//            WHERE jm.rencontre_id = $id
//        ";
//
//        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
//        $stmt->execute([]);
//
//        return $stmt->fetchAll();
//    }


    public function findByMatch($rencontre)
    {
        return $this->createQueryBuilder('j')
            ->where('j.rencontre = :rencontre')
            ->leftJoin('j.joueur', 'joueur')
            ->addSelect('joueur')
            ->setParameter('rencontre', $rencontre)
            ->getQuery()->getResult();
    }
    // /**
    //  * @return JoueurRencontre[] Returns an array of JoueurRencontre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JoueurRencontre
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
