<?php

namespace App\Repository;

use App\Entity\Faute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Faute|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faute|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faute[]    findAll()
 * @method Faute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FauteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Faute::class);
    }

     /**
      * @return Faute[] Returns an array of Faute objects
      */

    public function findCartonByColor($color)
    {
        return $this->createQueryBuilder('f')
            ->select('COUNT(*)')
            ->andWhere('f.carton = val')
            ->setParameter('val', $color)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findCartonByColorByMatch($color, $match)
    {
        return $this->createQueryBuilder('f')
            ->select('COUNT(*)')
            ->andWhere('f.carton = :color')
            ->setParameter('color', $color)
            ->andWhere('f.rencontre = :match')
            ->setParameter('match', $match)
            ->getQuery()
            ->getResult()
            ;
    }

        public function findByColorId($id)
    {
        $rawSql = "
             SELECT * FROM faute
             WHERE carton_id ='$id'
              ;
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute();

        return $stmt->rowCount();
    }


    public function findByMatch($rencontre)
    {
        return $this->createQueryBuilder('j')
            ->where('j.rencontre = :rencontre')
            ->leftJoin('j.joueur', 'joueur')
            ->addSelect('joueur')
            ->setParameter('rencontre', $rencontre)
            ->getQuery()->getScalarResult();
    }
    /*
    public function findOneBySomeField($value): ?Faute
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
