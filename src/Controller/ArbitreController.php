<?php

namespace App\Controller;

use App\Entity\Arbitre;
use App\Form\ArbitreType;
use App\Repository\ArbitreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/arbitre", name="referee")
 */
class ArbitreController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     * @param ArbitreRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ArbitreRepository $repository)
    {
        $referees = $repository->findAll();

        return $this->render('arbitre/index.html.twig', [
            'referees' => $referees,
            'title' => "Liste des arbitres"
        ]);
    }

    /**
     * @Route("/add", name="_add")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request, EntityManagerInterface $entityManager) {
        $player = new Arbitre();
        $form = $this->createForm(ArbitreType::class, $player);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($player);
            $entityManager->flush();
            $this->addFlash("success", "L'arbitre a bien été ajouté");
            return $this->redirectToRoute("referee_index");
        }

        return $this->render('arbitre/edit.html.twig', [
           'form' => $form->createView(),
            'title' => "Ajout d'un arbitre"
        ]);
    }
}
