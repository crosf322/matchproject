<?php

namespace App\Controller;

use App\Repository\ArbitreMatchRepository;
use App\Repository\ArbitreRepository;
use App\Repository\ButAdverseRepository;
use App\Repository\ButRepository;
use App\Repository\CartonRepository;
use App\Repository\ChangementRepository;
use App\Repository\FauteRepository;
use App\Repository\JoueurMatchRepository;
use App\Repository\JoueurRepository;
use App\Repository\PositionRepository;
use App\Repository\PosteRepository;
use App\Repository\RencontreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/statistiques", name="stats")
 */
class StatistiquesController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     */
    public function index (ButRepository $butRepository,
                           FauteRepository $fauteRepository,
                           CartonRepository $cartonRepository,
                           RencontreRepository $rencontreRepository,
                           ButAdverseRepository $butAdverseRepository,
                           ChangementRepository $changementRepository,
                           JoueurRepository $joueurRepository)
    {
        $dreamTeam = $rencontreRepository->findBestComboByPostePosition([1,1,2,2],[1,2,1,2]);

        $joueurs = [];

        if(isset($dreamTeam['joueur_1'])) {
            $joueurs = [
                'joueur_1' => $joueurRepository->find($dreamTeam['joueur_1']),
                'joueur_2' => $joueurRepository->find($dreamTeam['joueur_2']),
                'joueur_3' => $joueurRepository->find($dreamTeam['joueur_3']),
                'joueur_4' => $joueurRepository->find($dreamTeam['joueur_4']),
                'nbVictoires' => $dreamTeam['nb_victoire'],
            ];
        }

        return $this->render( 'statistiques/index.html.twig', [
            'dreamteam' => $joueurs,
            'temps_moy_joueur' => $changementRepository->tempsMoyenEquipe($joueurRepository),
            'nbTotalDeBut' => count( $butRepository->findAll() ),
            'nbJaune' => $fauteRepository->findByColorId( $cartonRepository->findOneBy( ['couleur' => 'jaune'] )->getId() ),
            'nbRouge' => $fauteRepository->findByColorId( $cartonRepository->findOneBy( ['couleur' => 'rouge'] )->getId() ),
            'victoire' => $this->ratioVictoire( $rencontreRepository),
        ] );
    }

    /**
     * @Route("/joueur/{id}", name="_joueur")
     *     requirements={"id"="\d+"},
     *     defaults={"id" = 0})
     */
    public function player (JoueurRepository $joueurRepository,
                            ButRepository $butRepository,
                            FauteRepository $fauteRepository,
                            Request $request)
    {
        $player = $joueurRepository->find($request->get( 'id' ));

        $participation = $joueurRepository->getListParticipation($player);

        $tempsDeJeux = $joueurRepository->tempTotalJoueur($player, $participation);

        $but = $butRepository->findBy( ['joueur' => $player,] );

        $jaune = $fauteRepository->findBy( [
            'joueur' => $player,
            'carton' => 1
        ] );

        $rouge = $fauteRepository->findBy( [
            'joueur' => $player,
            'carton' => 2
        ] );

        return $this->render( 'statistiques/player.html.twig', [
            'player' => $player,
            'controller_name' => 'StatistiquesController',
            'participation' => sizeof( $participation ),
            'but' => sizeof( $but ),
            'jaune' => sizeof( $jaune ),
            'rouge' => sizeof( $rouge ),
            'tempsDeJeux' => $tempsDeJeux] );
    }

    /**
     * @Route("/arbitre/{id}", name="_arbitre")
     *     requirements={"id"="\d+"},
     *     defaults={"id" = 0})
     */
    public function referee (ArbitreRepository $repository,
                             ArbitreMatchRepository $arbitreMatchRepository,
                             Request $request)
    {
        $referee = $repository->findOneBy( [
            'id' => $request->get( 'id' )
        ] );

        $arbitreMatch = $arbitreMatchRepository->findBy( [
            'arbitre' => $referee,
        ] );

        $totalMatchSuivi = sizeof( $arbitreMatch );

        $principal = 0;
        foreach ($arbitreMatch as $match) {
            if ($match->getEstPrincipal()) {
                $principal++;
            }
        }
        $assistant = $totalMatchSuivi - $principal;

        return $this->render( 'statistiques/referee.html.twig', [
            'referee' => $referee,
            'controller_name' => 'StatistiquesController',
            'principal' => $principal,
            'assistant' => $assistant,
        ] );
    }

    /**
     * @Route("/rencontre/{id}", name="_match")
     *     requirements={"id"="\d+"},
     *     defaults={"id" = 0})
     */
    public function match (RencontreRepository $rencontreRepository,
                           ArbitreMatchRepository $arbitreMatchRepository,
                           JoueurMatchRepository $joueurMatchRepository,
                           JoueurRepository $joueurRepository,
                           PosteRepository $posteRepository,
                           PositionRepository $positionRepository,
                           Request $request)
    {
        $match = $rencontreRepository->find($request->get( 'id' ));

        $listeJoueurs = $joueurMatchRepository->findByMatch( $match );
        $listeArbitre = $arbitreMatchRepository->findByMatch( $match );

        $events = $rencontreRepository->getAllEventsByMatch($match->getId(), $joueurRepository);

        $listePostes = $posteRepository->findOrderedList();
        $listePosition = $positionRepository->findOrderedList();

        return $this->render( 'statistiques/match.html.twig', [
            'evenements' => $events,
            'poste' => $listePostes,
            'position' => $listePosition,
            'match' => $match,
            'listeArbitres' => $listeArbitre,
            'listeJoueurs' => $listeJoueurs,
        ] );
    }

    /**
     * @param ButRepository $butRepository
     * @param RencontreRepository $rencontreRepository
     * @param int $idMatch
     */
    private function ratioVictoire(RencontreRepository $rencontreRepository): array
    {
        $scoreRencontres = $rencontreRepository->getAllScore();

        $victoires = 0;
        $nbMatch = 0;

        foreach ($scoreRencontres as $scoreRencontre) {
            if($scoreRencontre['score_equipe'] > $scoreRencontre['score_adverse']) {
                $victoires++;
            }
             $nbMatch++;
        }

        return [
            'nbVictoire' => $victoires,
            'ratio' => $victoires / $nbMatch *100
        ];

    }
}

