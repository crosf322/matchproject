<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArbitreMatchRepository")
 */
class ArbitreRencontre
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Arbitre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $arbitre;

    /**
     * @ORM\Column(type="boolean")
     */
    private $estPrincipal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rencontre", inversedBy="arbitreMatches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rencontre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArbitre(): ?Arbitre
    {
        return $this->arbitre;
    }

    public function setArbitre(?Arbitre $arbitre): self
    {
        $this->arbitre = $arbitre;

        return $this;
    }

    public function getRencontre(): ?Rencontre
    {
        return $this->rencontre;
    }

    public function setRencontre(?Rencontre $rencontre): self
    {
        $this->rencontre = $rencontre;

        return $this;
    }

    /**
     * @return bool
     */
    public function getEstPrincipal (): bool
    {
        return $this->estPrincipal;
    }

    /**
     * @param bool $estPrincipal
     */
    public function setEstPrincipal ($estPrincipal): void
    {
        $this->estPrincipal = $estPrincipal;
    }

    public function __toString() {
        return $this->getArbitre()->getPrenom() . " " . $this->getArbitre()->getNom();
    }
}
