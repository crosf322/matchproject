<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JoueurRepository")
 */
class Joueur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Faute", mappedBy="joueur")
     */
    private $fautes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $estTitulaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Poste")
     * @ORM\JoinColumn(nullable=false)
     */
    private $postePredilection;

    public function __construct()
    {
        $this->fautes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Faute[]
     */
    public function getFautes(): Collection
    {
        return $this->fautes;
    }

    public function addFaute(Faute $faute): self
    {
        if (!$this->fautes->contains($faute)) {
            $this->fautes[] = $faute;
            $faute->addJoueur($this);
        }

        return $this;
    }

    public function removeFaute(Faute $faute): self
    {
        if ($this->fautes->contains($faute)) {
            $this->fautes->removeElement($faute);
            $faute->removeJoueur($this);
        }

        return $this;
    }

    public function __toString() {
        return $this->nom . " " . $this->prenom;
    }

    public function getEstTitulaire(): ?int
    {
        return $this->estTitulaire;
    }

    public function setEstTitulaire(int $estTitulaire): self
    {
        $this->estTitulaire = $estTitulaire;

        return $this;
    }

    public function getPostePredilection(): ?Poste
    {
        return $this->postePredilection;
    }

    public function setPostePredilection(?Poste $postePredilection): self
    {
        $this->postePredilection = $postePredilection;

        return $this;
    }
}
