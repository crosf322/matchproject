<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChangementRepository")
 */
class Changement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur")
     * @ORM\JoinColumn(nullable=true)
     */
    private $entreeJoueur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sortieJoueur;

    /**
     * @ORM\Column(type="integer")
     */
    private $temps;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tempsDeSortie;

    /**
     * @ORM\ManyToOne(targetEntity="Rencontre", inversedBy="changements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rencontre;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntreeJoueur(): ?Joueur
    {
        return $this->entreeJoueur;
    }

    public function setEntreeJoueur(?Joueur $entreeJoueur): self
    {
        $this->entreeJoueur = $entreeJoueur;

        return $this;
    }

    public function getSortieJoueur(): ?Joueur
    {
        return $this->sortieJoueur;
    }

    public function setSortieJoueur(?Joueur $sortieJoueur): self
    {
        $this->sortieJoueur = $sortieJoueur;

        return $this;
    }

    public function getTemps(): ?int
    {
        return $this->temps;
    }

    public function setTemps(int $temps): self
    {
        $this->temps = $temps;

        return $this;
    }

    public function getTempsDeSortie(): ?int
    {
        return $this->tempsDeSortie;
    }

    public function setTempsDeSortie(?int $tempsDeSortie): self
    {
        $this->tempsDeSortie = $tempsDeSortie;

        return $this;
    }

    public function getRencontre(): ?Rencontre
    {
        return $this->rencontre;
    }

    public function setRencontre(?Rencontre $rencontre): self
    {
        $this->rencontre = $rencontre;

        return $this;
    }

    public function __toString() {
        return "changement";
    }
}
