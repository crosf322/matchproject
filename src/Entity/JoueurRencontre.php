<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JoueurMatchRepository")
 */
class JoueurRencontre implements \JsonSerializable
{


    public const POSTES = [
        0 => "",
        1 => "Avant",
        2 => "Ailier",
        3 => "Milieu offensif",
        4 => "Milieu",
        5 => "Milieu défensif",
        6 => "Arrière",
        7 => "Gardien"
    ];

    public const PLACEMENTS = [
        0 => "",
        1 => "Centre",
        2 => "Droit",
        3 => "Gauche"
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $numero;

    /**
     * @ORM\Column(type="boolean")
     */
    private $estRemplacant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Joueur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $joueur;

    /**
     * @ORM\ManyToOne(targetEntity="Rencontre", inversedBy="joueurMatches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rencontre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Poste")
     */
    private $poste;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Position")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getEstRemplacant(): ?bool
    {
        return $this->estRemplacant;
    }

    public function setEstRemplacant(bool $estRemplacant): self
    {
        $this->estRemplacant = $estRemplacant;

        return $this;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getRencontre(): ?Rencontre
    {
        return $this->rencontre;
    }

    public function setRencontre(?Rencontre $rencontre): self
    {
        $this->rencontre = $rencontre;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->joueur->getId(),
            'name' => $this->joueur->getNom() . " " . $this->joueur->getPrenom(),
            'poste' => ($this->getPoste() ? $this->getPoste()->getId() : 0),
            'position' => ($this->getPosition() ? $this->getPosition()->getId() : 0),
            'numero' => $this->getNumero(),
            'estRemplacant' => $this->estRemplacant,
            'status' => 0,
        ];
    }

    public function getPoste(): ?Poste
    {
        return $this->poste;
    }

    public function setPoste(?Poste $poste): self
    {
        $this->poste = $poste;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function __toString()
    {
        return $this->getJoueur()->getPrenom() . " " . $this->getJoueur()->getNom();
    }
}
