<?php

namespace App\Utils;


use App\Entity\Rencontre;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class RencontreUtils
{
    public function getTimeSinceBeginning(Rencontre $rencontre) {
        $date = new \DateTime();
        $date->add(new \DateInterval(('PT2H')));
        $prem = $rencontre->getPremiereMiTemps();

        if($prem) {
            $da = new \DateTime($rencontre->getDate()->format('Y/m/d H:i:s'));
            $da->add(new \DateInterval('PT' . $prem . 'M')); // 18h05

            if($rencontre->getSecondeMiTemps()) {
                $dateMiTemps = new \DateTime($rencontre->getDate()->format('Y/m/d H:i:s'));
                $dateMiTemps->add(new \DateInterval('PT' . ($prem+45) . 'M')); // 18h50

                dump($dateMiTemps);

                $sDate = new \DateTime($rencontre->getDate()->format('Y/m/d H:i:s'));
                $sDate->add(new \DateInterval('PT' . $rencontre->getSecondeMiTemps() . 'M')); // 19h05 // debut secondemi temps

                dump($sDate);
                $sTime = $this->calcMinutesBetween($sDate, $dateMiTemps); // ==> 15 minutes de pub

                dump($sTime);

                return $this->calcMinutesBetween($da, $date) - $sTime;

            }

            return $this->calcMinutesBetween($da, $date);
        } else {
            $dat = new \DateTime($rencontre->getDate()->format('Y/m/d H:i:s'));
            return $this->calcMinutesBetween($date, $dat);
        }



    }

    private function calcMinutesBetween(\DateTime $date, \DateTime $date2) {
        $inter = $date->diff($date2);

        return (($inter->h*60) + $inter->i);
    }
}