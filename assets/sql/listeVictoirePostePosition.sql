DROP PROCEDURE IF EXISTS dreamTeam;

DELIMITER $$
CREATE PROCEDURE dreamTeam(IN post_id_1 INT, IN posit_id_1 INT,IN post_id_2 INT, IN posit_id_2 INT,IN post_id_3 INT, IN posit_id_3 INT,IN post_id_4 INT, IN posit_id_4 INT)

BEGIN

SELECT distinct jr.rencontre_id
FROM joueur_rencontre jr
WHERE jr.rencontre_id IN (
	SELECT r.id
	FROM rencontre r
	LEFT JOIN 	(
				SELECT r.id AS m_id, count(b.id) AS score_equipe
				FROM  rencontre r 
				INNER JOIN but b
					ON b.rencontre_id = r.id
				GROUP BY r.id
			) b
			ON b.m_id = r.id 
	LEFT JOIN 	(
				SELECT r.id AS m_id,count(ba.id) AS score_adverse
				FROM  rencontre r 
				INNER JOIN but_adverse ba
					ON ba.rencontre_id = r.id
				GROUP BY r.id
			) ba
			ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)	
				
	ORDER BY r.id
) 
AND (
	(jr.poste_id = post_id_1 AND jr.position_id = posit_id_1) OR
	(jr.poste_id = post_id_2 AND jr.position_id = posit_id_2) OR
	(jr.poste_id = post_id_3 AND jr.position_id = posit_id_3) OR
	(jr.poste_id = post_id_4 AND jr.position_id = posit_id_4) 
)



;
END $$
DELIMITER ;