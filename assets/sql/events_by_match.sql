USE `match`;

DROP PROCEDURE IF EXISTS events_by_match;

DELIMITER $$
CREATE PROCEDURE events_by_match(IN match_id INT)

SELECT main.evenement, main.arg1, main.arg2, main.arg3, main.temps
FROM
	(
	SELECT 'but' AS evenement,b.joueur_id AS arg1, NULL AS arg2, NULL AS arg3, b.temps
	FROM but b
	WHERE b.rencontre_id = match_id
	
	UNION 
	
	SELECT 'changement' AS evenement,ch.entree_joueur_id AS arg1, ch.sortie_joueur_id AS arg2, ch.temps_de_sortie AS arg3, ch.temps
	FROM changement ch
	WHERE ch.rencontre_id = match_id
	
	UNION 
	
	SELECT 'but_adverse' AS evenement, NULL AS arg1, NULL AS arg2, NULL AS arg3, ba.temps
	FROM but_adverse ba
	WHERE ba.rencontre_id = match_id
) main

ORDER BY main.temps
;

END $$
DELIMITER ;

