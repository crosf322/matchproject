-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour match
DROP DATABASE IF EXISTS `match`;
CREATE DATABASE IF NOT EXISTS `match` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `match`;

-- Listage de la structure de la table match. arbitre
DROP TABLE IF EXISTS `arbitre`;
CREATE TABLE IF NOT EXISTS `arbitre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationalite` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.arbitre : ~6 rows (environ)
/*!40000 ALTER TABLE `arbitre` DISABLE KEYS */;
REPLACE INTO `arbitre` (`id`, `nom`, `nationalite`, `prenom`) VALUES
	(1, 'Jones', 'Peruvienne', 'Indiana'),
	(2, 'Daudet', 'Georgien', 'Alphonse'),
	(3, 'Cerdant', 'Turque', 'Marcel'),
	(4, 'Turpin', 'Francais', 'Clément'),
	(5, 'Chapron', 'Francais', 'Tony'),
	(6, 'Collina', 'Italien', 'Perluigi');
/*!40000 ALTER TABLE `arbitre` ENABLE KEYS */;

-- Listage de la structure de la table match. arbitre_rencontre
DROP TABLE IF EXISTS `arbitre_rencontre`;
CREATE TABLE IF NOT EXISTS `arbitre_rencontre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arbitre_id` int(11) NOT NULL,
  `rencontre_id` int(11) NOT NULL,
  `est_principal` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D6FCB014943A5F0` (`arbitre_id`),
  KEY `IDX_D6FCB0146CFC0818` (`rencontre_id`),
  CONSTRAINT `FK_D6FCB0146CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`),
  CONSTRAINT `FK_D6FCB014943A5F0` FOREIGN KEY (`arbitre_id`) REFERENCES `arbitre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.arbitre_rencontre : ~19 rows (environ)
/*!40000 ALTER TABLE `arbitre_rencontre` DISABLE KEYS */;
REPLACE INTO `arbitre_rencontre` (`id`, `arbitre_id`, `rencontre_id`, `est_principal`) VALUES
	(1, 1, 1, 1),
	(2, 2, 1, 0),
	(3, 3, 1, 0),
	(4, 1, 2, 0),
	(5, 2, 2, 1),
	(6, 3, 2, 0),
	(7, 1, 3, 1),
	(8, 2, 3, 0),
	(9, 3, 3, 0),
	(10, 1, 4, 0),
	(11, 2, 4, 0),
	(12, 3, 4, 1),
	(13, 1, 6, 1),
	(14, 3, 6, 0),
	(15, 2, 6, 0),
	(16, 1, 6, 1),
	(17, 1, 5, 1),
	(18, 2, 5, 0),
	(19, 3, 5, 0);
/*!40000 ALTER TABLE `arbitre_rencontre` ENABLE KEYS */;

-- Listage de la structure de la table match. but
DROP TABLE IF EXISTS `but`;
CREATE TABLE IF NOT EXISTS `but` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joueur_id` int(11) NOT NULL,
  `rencontre_id` int(11) DEFAULT NULL,
  `temps` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B132FECAA9E2D76C` (`joueur_id`),
  KEY `IDX_B132FECA6CFC0818` (`rencontre_id`),
  CONSTRAINT `FK_B132FECA6CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`),
  CONSTRAINT `FK_B132FECAA9E2D76C` FOREIGN KEY (`joueur_id`) REFERENCES `joueur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.but : ~7 rows (environ)
/*!40000 ALTER TABLE `but` DISABLE KEYS */;
REPLACE INTO `but` (`id`, `joueur_id`, `rencontre_id`, `temps`) VALUES
	(1, 2, 1, 11),
	(2, 12, 1, 23),
	(3, 1, 1, 45),
	(4, 4, 3, 12),
	(5, 6, 3, 64),
	(6, 12, 4, 12),
	(7, 4, 4, 24);
/*!40000 ALTER TABLE `but` ENABLE KEYS */;

-- Listage de la structure de la table match. but_adverse
DROP TABLE IF EXISTS `but_adverse`;
CREATE TABLE IF NOT EXISTS `but_adverse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rencontre_id` int(11) NOT NULL,
  `temps` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F867B8E56CFC0818` (`rencontre_id`),
  CONSTRAINT `FK_F867B8E56CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.but_adverse : ~5 rows (environ)
/*!40000 ALTER TABLE `but_adverse` DISABLE KEYS */;
REPLACE INTO `but_adverse` (`id`, `rencontre_id`, `temps`) VALUES
	(1, 1, 18),
	(2, 2, 12),
	(3, 2, 89),
	(4, 3, 27),
	(5, 4, 12);
/*!40000 ALTER TABLE `but_adverse` ENABLE KEYS */;

-- Listage de la structure de la table match. carton
DROP TABLE IF EXISTS `carton`;
CREATE TABLE IF NOT EXISTS `carton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `couleur` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.carton : ~2 rows (environ)
/*!40000 ALTER TABLE `carton` DISABLE KEYS */;
REPLACE INTO `carton` (`id`, `couleur`) VALUES
	(1, 'jaune'),
	(2, 'rouge');
/*!40000 ALTER TABLE `carton` ENABLE KEYS */;

-- Listage de la structure de la table match. changement
DROP TABLE IF EXISTS `changement`;
CREATE TABLE IF NOT EXISTS `changement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entree_joueur_id` int(11) DEFAULT NULL,
  `sortie_joueur_id` int(11) NOT NULL,
  `rencontre_id` int(11) NOT NULL,
  `temps` int(11) NOT NULL,
  `temps_de_sortie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_964C71E2BF038CA6` (`entree_joueur_id`),
  KEY `IDX_964C71E2EE63D9DE` (`sortie_joueur_id`),
  KEY `IDX_964C71E26CFC0818` (`rencontre_id`),
  CONSTRAINT `FK_964C71E26CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`),
  CONSTRAINT `FK_964C71E2BF038CA6` FOREIGN KEY (`entree_joueur_id`) REFERENCES `joueur` (`id`),
  CONSTRAINT `FK_964C71E2EE63D9DE` FOREIGN KEY (`sortie_joueur_id`) REFERENCES `joueur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.changement : ~7 rows (environ)
/*!40000 ALTER TABLE `changement` DISABLE KEYS */;
REPLACE INTO `changement` (`id`, `entree_joueur_id`, `sortie_joueur_id`, `rencontre_id`, `temps`, `temps_de_sortie`) VALUES
	(1, 12, 2, 1, 17, NULL),
	(2, 2, 1, 1, 19, NULL),
	(3, NULL, 2, 1, 20, 25),
	(4, NULL, 2, 1, 36, NULL),
	(5, 3, 12, 1, 42, NULL),
	(6, 12, 2, 1, 45, NULL),
	(7, NULL, 6, 1, 50, 40);
/*!40000 ALTER TABLE `changement` ENABLE KEYS */;

-- Listage de la structure de la procédure match. dreamTeam
DROP PROCEDURE IF EXISTS `dreamTeam`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `dreamTeam`(
			IN post_id_1 INT, IN posit_id_1 INT,
			IN post_id_2 INT, IN posit_id_2 INT,
			IN post_id_3 INT, IN posit_id_3 INT,
			IN post_id_4 INT, IN posit_id_4 INT
			)
BEGIN
	
SELECT count(jr1.match_id) AS nb_victoire, jr1.joueur_id AS joueur_1, jr2.joueur_id AS joueur_2, jr3.joueur_id AS joueur_3, jr4.joueur_id AS joueur_4
FROM (
	SELECT rencontre_id AS match_id, joueur_id 
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_1, posit_id_1)
	) AS jr1
LEFT JOIN (
	SELECT rencontre_id AS match_id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_2, posit_id_2)
	) AS jr2
	ON jr1.match_id = jr2.match_id
LEFT JOIN (
	SELECT rencontre_id AS match_id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_3, posit_id_3)
	) AS jr3
	ON jr1.match_id = jr3.match_id
LEFT JOIN (
	SELECT rencontre_id AS match_id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_4, posit_id_4)
	) AS jr4
	ON jr1.match_id = jr4.match_id
	
WHERE jr1.match_id IN (
	SELECT r.id
	FROM rencontre r
	LEFT JOIN 	(
		SELECT r.id AS m_id, count(b.id) AS score_equipe
		FROM  rencontre r 
		INNER JOIN but b
			ON b.rencontre_id = r.id
		GROUP BY r.id
		) b
	ON b.m_id = r.id 
	LEFT JOIN 	(
		SELECT r.id AS m_id,count(ba.id) AS score_adverse
		FROM  rencontre r 
		INNER JOIN but_adverse ba
			ON ba.rencontre_id = r.id
		GROUP BY r.id
		) ba
	ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)
)

GROUP BY joueur_1, joueur_2, joueur_3, joueur_4
ORDER BY nb_victoire DESC
LIMIT 1
;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. dreamTeam3
DROP PROCEDURE IF EXISTS `dreamTeam3`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `dreamTeam3`(IN post_id_1 INT, IN posit_id_1 INT,IN post_id_2 INT, IN posit_id_2 INT,IN post_id_3 INT, IN posit_id_3 INT)
BEGIN

SELECT *
FROM joueur_rencontre jr
WHERE jr.rencontre_id IN (
	SELECT r.id
	FROM rencontre r
	LEFT JOIN 	(
				SELECT r.id AS m_id, count(b.id) AS score_equipe
				FROM  rencontre r 
				INNER JOIN but b
					ON b.rencontre_id = r.id
				GROUP BY r.id
			) b
			ON b.m_id = r.id 
	LEFT JOIN 	(
				SELECT r.id AS m_id,count(ba.id) AS score_adverse
				FROM  rencontre r 
				INNER JOIN but_adverse ba
					ON ba.rencontre_id = r.id
				GROUP BY r.id
			) ba
			ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)	
				
	ORDER BY r.id
) 
AND (
	(jr.poste_id = post_id_1 AND jr.position_id = posit_id_1) OR
	(jr.poste_id = post_id_2 AND jr.position_id = posit_id_2) OR
	(jr.poste_id = post_id_3 AND jr.position_id = posit_id_3)
)

;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. dreamTeam4
DROP PROCEDURE IF EXISTS `dreamTeam4`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `dreamTeam4`(
	IN `post_id_1` INT,
	IN `posit_id_1` INT,
	IN `post_id_2` INT,
	IN `posit_id_2` INT,
	IN `post_id_3` INT,
	IN `posit_id_3` INT,
	IN `post_id_4` INT,
	IN `posit_id_4` INT
			
)
BEGIN
	
SELECT jr1.id, jr1.joueur_id, jr2.joueur_id, jr3.joueur_id, jr4.joueur_id
FROM (
	SELECT rencontre_id AS id, joueur_id 
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_1, posit_id_1)
	) AS jr1
LEFT JOIN (
	SELECT rencontre_id AS id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_2, posit_id_2)
	) AS jr2
	ON jr1.id = jr2.id
LEFT JOIN (
	SELECT rencontre_id AS id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_3, posit_id_3)
	) AS jr3
	ON jr1.id = jr3.id
LEFT JOIN (
	SELECT rencontre_id AS id, joueur_id
	FROM joueur_rencontre 
	WHERE (poste_id, position_id) = (post_id_4, posit_id_4)
	) AS jr4
	ON jr1.id = jr4.id
	
WHERE jr1.id IN (
	SELECT r.id
	FROM rencontre r
	LEFT JOIN 	(
		SELECT r.id AS m_id, count(b.id) AS score_equipe
		FROM  rencontre r 
		INNER JOIN but b
			ON b.rencontre_id = r.id
		GROUP BY r.id
		) b
	ON b.m_id = r.id 
	LEFT JOIN 	(
		SELECT r.id AS m_id,count(ba.id) AS score_adverse
		FROM  rencontre r 
		INNER JOIN but_adverse ba
			ON ba.rencontre_id = r.id
		GROUP BY r.id
		) ba
	ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)
)
	 
ORDER BY jr1.id
;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. dreamTeam4ByIdMatch
DROP PROCEDURE IF EXISTS `dreamTeam4ByIdMatch`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `dreamTeam4ByIdMatch`(
			IN post_id_1 INT, IN posit_id_1 INT,
			IN post_id_2 INT, IN posit_id_2 INT,
			IN post_id_3 INT, IN posit_id_3 INT,
			IN post_id_4 INT, IN posit_id_4 INT
			)
BEGIN

SELECT m.id
	FROM rencontre m;

SELECT 
	(
	SELECT distinct jr.rencontre_id 
	FROM joueur_rencontre jr
	WHERE jr.rencontre_id = @rencontre
	) AS 'match_id',
	(
	SELECT jr.joueur_id 
	FROM joueur_rencontre jr
	WHERE jr.rencontre_id = @rencontre
	AND jr.poste_id = post_id_1
	AND jr.position_id = posit_id_1) AS '1', 
	(
	SELECT jr.joueur_id 
	FROM joueur_rencontre jr
	WHERE jr.rencontre_id =  @rencontre
	AND jr.poste_id = post_id_2
	AND jr.position_id = posit_id_2) AS '2', 
	(
	SELECT jr.joueur_id 
	FROM joueur_rencontre jr
	WHERE jr.rencontre_id =  @rencontre
	AND jr.poste_id = post_id_3
	AND jr.position_id = posit_id_3) AS '3', 
	(
	SELECT jr.joueur_id 
	FROM joueur_rencontre jr
	WHERE jr.rencontre_id =  @rencontre
	AND jr.poste_id = post_id_4
	AND jr.position_id = posit_id_4) AS '4'


	

;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. envents_by_match
DROP PROCEDURE IF EXISTS `envents_by_match`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `envents_by_match`(IN match_id INT)
SELECT main.evenement, main.arg1, main.arg2, main.arg3, main.temps
FROM
	(
	SELECT 'but' AS evenement,b.joueur_id AS arg1, NULL AS arg2, NULL AS arg3, b.temps
	FROM but b
	WHERE b.rencontre_id = match_id
	
	UNION 
	
	SELECT 'changement' AS evenement,ch.entree_joueur_id AS arg1, ch.sortie_joueur_id AS arg2, ch.temps_de_sortie AS arg3, ch.temps
	FROM changement ch
	WHERE ch.rencontre_id = match_id
	
	UNION 
	
	SELECT 'but_adverse' AS evenement, NULL AS arg1, NULL AS arg2, NULL AS arg3, ba.temps
	FROM but_adverse ba
	WHERE ba.rencontre_id = match_id
) main

ORDER BY main.temps
;//
DELIMITER ;

-- Listage de la structure de la procédure match. eventJoueurMatch
DROP PROCEDURE IF EXISTS `eventJoueurMatch`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `eventJoueurMatch`(IN match_id INT, IN joueur_id INT)
BEGIN
    SELECT *
	FROM changement ch
	WHERE entree_joueur_id = joueur_id
	OR sortie_joueur_id = joueur_id
	having rencontre_id = match_id;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. events_by_match
DROP PROCEDURE IF EXISTS `events_by_match`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `events_by_match`(IN match_id INT)
SELECT main.evenement, main.arg1, main.arg2, main.arg3, main.temps
FROM
(
SELECT 'but' AS evenement,b.joueur_id AS arg1, NULL AS arg2, NULL AS arg3, b.temps
FROM but b
WHERE b.rencontre_id = 1

UNION 

SELECT 'changement' AS evenement,ch.entree_joueur_id AS arg1, ch.sortie_joueur_id AS arg2, ch.temps_de_sortie AS arg3, ch.temps
FROM changement ch
WHERE ch.rencontre_id = 1

UNION 

SELECT 'but_adverse' AS evenement, NULL AS arg1, NULL AS arg2, NULL AS arg3, ba.temps
FROM but_adverse ba
WHERE ba.rencontre_id = 1
) main

ORDER BY main.temps
;//
DELIMITER ;

-- Listage de la structure de la table match. faute
DROP TABLE IF EXISTS `faute`;
CREATE TABLE IF NOT EXISTS `faute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joueur_id` int(11) DEFAULT NULL,
  `arbitre_id` int(11) NOT NULL,
  `rencontre_id` int(11) DEFAULT NULL,
  `sanction_id` int(11) NOT NULL,
  `temps` int(11) NOT NULL,
  `carton_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_777B6608A9E2D76C` (`joueur_id`),
  KEY `IDX_777B6608943A5F0` (`arbitre_id`),
  KEY `IDX_777B66086CFC0818` (`rencontre_id`),
  KEY `IDX_777B660896E0C11A` (`sanction_id`),
  KEY `IDX_777B66082D79FBB1` (`carton_id`),
  CONSTRAINT `FK_777B66082D79FBB1` FOREIGN KEY (`carton_id`) REFERENCES `carton` (`id`),
  CONSTRAINT `FK_777B66086CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`),
  CONSTRAINT `FK_777B6608943A5F0` FOREIGN KEY (`arbitre_id`) REFERENCES `arbitre` (`id`),
  CONSTRAINT `FK_777B660896E0C11A` FOREIGN KEY (`sanction_id`) REFERENCES `sanction` (`id`),
  CONSTRAINT `FK_777B6608A9E2D76C` FOREIGN KEY (`joueur_id`) REFERENCES `joueur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.faute : ~9 rows (environ)
/*!40000 ALTER TABLE `faute` DISABLE KEYS */;
REPLACE INTO `faute` (`id`, `joueur_id`, `arbitre_id`, `rencontre_id`, `sanction_id`, `temps`, `carton_id`) VALUES
	(1, 1, 3, 1, 2, 12, 2),
	(2, 1, 3, 1, 2, 20, 1),
	(3, 1, 2, 1, 1, 34, 1),
	(4, 6, 2, 1, 1, 34, 1),
	(5, 4, 3, 1, 2, 12, 2),
	(6, 4, 3, 1, 2, 12, 2),
	(7, 4, 3, 1, 2, 12, 2),
	(8, 1, 2, 4, 2, 12, 1),
	(9, 2, 3, 3, 3, 16, 2);
/*!40000 ALTER TABLE `faute` ENABLE KEYS */;

-- Listage de la structure de la procédure match. getScoreMatch
DROP PROCEDURE IF EXISTS `getScoreMatch`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getScoreMatch`()
BEGIN

SELECT r.id, IF(b.score_equipe IS NULL,0,b.score_equipe) AS score_equipe, IF(ba.score_adverse IS NULL,0,ba.score_adverse) AS score_adverse
FROM rencontre r
LEFT JOIN 	(
			SELECT r.id AS m_id, count(b.id) AS score_equipe
			FROM  rencontre r 
			INNER JOIN but b
				ON b.rencontre_id = r.id
			GROUP BY r.id
		) b
		ON b.m_id = r.id 

LEFT JOIN 	(
			SELECT r.id AS m_id,count(ba.id) AS score_adverse
			FROM  rencontre r 
			INNER JOIN but_adverse ba
				ON ba.rencontre_id = r.id
			GROUP BY r.id
		) ba
		ON ba.m_id = r.id 

;
		
			

END//
DELIMITER ;

-- Listage de la structure de la procédure match. getWinMatch
DROP PROCEDURE IF EXISTS `getWinMatch`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `getWinMatch`()
BEGIN

SELECT r.id
	FROM rencontre r
	LEFT JOIN 	(
		SELECT r.id AS m_id, count(b.id) AS score_equipe
		FROM  rencontre r 
		INNER JOIN but b
			ON b.rencontre_id = r.id
		GROUP BY r.id
		) b
	ON b.m_id = r.id 
	LEFT JOIN 	(
		SELECT r.id AS m_id,count(ba.id) AS score_adverse
		FROM  rencontre r 
		INNER JOIN but_adverse ba
			ON ba.rencontre_id = r.id
		GROUP BY r.id
		) ba
	ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)
	
	;
END//
DELIMITER ;

-- Listage de la structure de la procédure match. get_score_by_match
DROP PROCEDURE IF EXISTS `get_score_by_match`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_score_by_match`(IN match_id INT)
BEGIN

SELECT b.rencontre_id,ba.rencontre_id
FROM but b
JOIN but_adverse ba ON b.rencontre_id = ba.rencontre_id

;
END//
DELIMITER ;

-- Listage de la structure de la table match. joueur
DROP TABLE IF EXISTS `joueur`;
CREATE TABLE IF NOT EXISTS `joueur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prenom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `est_titulaire` tinyint(1) DEFAULT NULL,
  `poste_predilection_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD71A9C511B225D5` (`poste_predilection_id`),
  CONSTRAINT `FK_FD71A9C511B225D5` FOREIGN KEY (`poste_predilection_id`) REFERENCES `poste` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.joueur : ~15 rows (environ)
/*!40000 ALTER TABLE `joueur` DISABLE KEYS */;
REPLACE INTO `joueur` (`id`, `prenom`, `nom`, `est_titulaire`, `poste_predilection_id`) VALUES
	(1, 'Bob', 'Morane', 1, NULL),
	(2, 'Bob', 'Marley', 1, NULL),
	(3, 'Stephen', 'King', 1, NULL),
	(4, 'Alfred', 'Hitchcock', 1, NULL),
	(5, 'Léodagan', 'De Carmelide', 1, NULL),
	(6, 'Eric', 'Cartman', 1, NULL),
	(7, 'Lara', 'Croft', 1, NULL),
	(8, 'Laura', 'Lone', 1, NULL),
	(9, 'Speedy', 'Gonzales', 1, NULL),
	(10, 'Stan', 'March', 1, NULL),
	(11, 'Kyle', 'Broflofsky', 1, NULL),
	(12, 'Kenny', 'McCormick', 1, NULL),
	(13, 'Lego', 'Lass', 1, NULL),
	(14, 'Stanley', 'Cubrick', 1, NULL),
	(17, 'Patty', 'Smith', 1, 4);
/*!40000 ALTER TABLE `joueur` ENABLE KEYS */;

-- Listage de la structure de la table match. joueur_rencontre
DROP TABLE IF EXISTS `joueur_rencontre`;
CREATE TABLE IF NOT EXISTS `joueur_rencontre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `joueur_id` int(11) NOT NULL,
  `rencontre_id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `est_remplacant` tinyint(1) NOT NULL,
  `poste_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B852AFB5A9E2D76C` (`joueur_id`),
  KEY `IDX_B852AFB56CFC0818` (`rencontre_id`),
  KEY `IDX_B852AFB5A0905086` (`poste_id`),
  KEY `IDX_B852AFB5DD842E46` (`position_id`),
  CONSTRAINT `FK_B852AFB56CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`),
  CONSTRAINT `FK_B852AFB5A0905086` FOREIGN KEY (`poste_id`) REFERENCES `poste` (`id`),
  CONSTRAINT `FK_B852AFB5A9E2D76C` FOREIGN KEY (`joueur_id`) REFERENCES `joueur` (`id`),
  CONSTRAINT `FK_B852AFB5DD842E46` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.joueur_rencontre : ~36 rows (environ)
/*!40000 ALTER TABLE `joueur_rencontre` DISABLE KEYS */;
REPLACE INTO `joueur_rencontre` (`id`, `joueur_id`, `rencontre_id`, `numero`, `est_remplacant`, `poste_id`, `position_id`) VALUES
	(1, 4, 1, 1, 0, 2, 2),
	(2, 1, 1, 2, 0, 1, 1),
	(3, 2, 1, 3, 0, 1, 2),
	(4, 3, 2, 4, 0, 1, 2),
	(5, 6, 2, 41, 0, 2, 1),
	(6, 1, 2, 11, 1, 1, 1),
	(7, 2, 2, 16, 0, 2, 2),
	(8, 10, 1, 4, 0, 2, 1),
	(11, 1, 3, 41, 0, 1, 1),
	(12, 2, 3, 18, 0, 1, 2),
	(13, 14, 3, 24, 0, 2, 1),
	(14, 6, 3, 12, 0, 2, 2),
	(15, 2, 4, 13, 0, 1, 1),
	(16, 5, 4, 11, 0, 1, 2),
	(17, 10, 4, 9, 0, 2, 1),
	(18, 1, 4, 7, 0, 2, 2),
	(19, 12, 2, 21, 0, 4, 4),
	(20, 1, 5, 1, 0, 2, 2),
	(21, 2, 5, 1, 0, 2, 1),
	(22, 1, 6, 5, 0, 5, 3),
	(23, 14, 6, 12, 0, 2, 1),
	(24, 13, 6, 6, 0, 1, 1),
	(25, 12, 6, 17, 0, 1, 2),
	(26, 5, 6, 8, 0, 4, 4),
	(27, 7, 6, 3, 0, 6, 2),
	(28, 9, 6, 4, 0, 6, 1),
	(29, 8, 6, 3, 0, 2, 2),
	(30, 3, 6, 2, 0, NULL, 2),
	(31, 2, 6, 11, 0, NULL, 2),
	(32, 3, 5, 3, 0, 2, 3),
	(33, 4, 5, 4, 0, 1, 2),
	(34, 5, 5, 5, 0, 1, 1),
	(35, 6, 5, 6, 0, 5, 3),
	(36, 7, 5, 7, 0, NULL, 2),
	(37, 8, 5, 8, 0, 6, 2),
	(38, 9, 5, 10, 0, 4, 4);
/*!40000 ALTER TABLE `joueur_rencontre` ENABLE KEYS */;

-- Listage de la structure de la table match. position
DROP TABLE IF EXISTS `position`;
CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.position : ~4 rows (environ)
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
REPLACE INTO `position` (`id`, `nom`) VALUES
	(1, 'Droit'),
	(2, 'Centre'),
	(3, 'Gauche'),
	(4, ' ');
/*!40000 ALTER TABLE `position` ENABLE KEYS */;

-- Listage de la structure de la table match. poste
DROP TABLE IF EXISTS `poste`;
CREATE TABLE IF NOT EXISTS `poste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.poste : ~6 rows (environ)
/*!40000 ALTER TABLE `poste` DISABLE KEYS */;
REPLACE INTO `poste` (`id`, `nom`) VALUES
	(1, 'Allier'),
	(2, 'Avant'),
	(3, 'Defensseur'),
	(4, 'Gardien'),
	(5, 'Milieu'),
	(6, 'Milieu_deffensif');
/*!40000 ALTER TABLE `poste` ENABLE KEYS */;

-- Listage de la structure de la table match. rencontre
DROP TABLE IF EXISTS `rencontre`;
CREATE TABLE IF NOT EXISTS `rencontre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `capitaine_id` int(11) DEFAULT NULL,
  `suppleant_id` int(11) DEFAULT NULL,
  `equipe_adverse` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lieu` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `type_maillot` tinyint(1) NOT NULL,
  `fin_match` int(11) DEFAULT NULL,
  `premiere_mi_temps` int(11) DEFAULT NULL,
  `seconde_mi_temps` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_460C35ED67A3C51B` (`suppleant_id`),
  KEY `IDX_460C35ED2A10D79E` (`capitaine_id`),
  CONSTRAINT `FK_460C35ED2A10D79E` FOREIGN KEY (`capitaine_id`) REFERENCES `joueur_rencontre` (`id`),
  CONSTRAINT `FK_460C35ED67A3C51B` FOREIGN KEY (`suppleant_id`) REFERENCES `joueur_rencontre` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.rencontre : ~6 rows (environ)
/*!40000 ALTER TABLE `rencontre` DISABLE KEYS */;
REPLACE INTO `rencontre` (`id`, `capitaine_id`, `suppleant_id`, `equipe_adverse`, `lieu`, `date`, `type_maillot`, `fin_match`, `premiere_mi_temps`, `seconde_mi_temps`) VALUES
	(1, 2, 6, 'Angola', 'Bratislava', '2019-04-21 19:43:00', 1, 90, 45, 90),
	(2, 4, 4, 'Angola', 'Bratislava', '2019-04-21 19:43:00', 1, 90, 45, 90),
	(3, 12, 1, 'Islande', 'Liverpool', '2019-05-01 00:00:00', 1, 90, 45, 90),
	(4, 1, 7, 'Bobbilonia', 'Amsterdam', '2019-04-28 13:02:15', 0, 90, 45, 90),
	(5, 21, 32, 'Canada', 'Vladivostok', '2019-05-06 13:20:00', 1, 90, 45, 90),
	(6, 30, 27, 'Canada', 'Madrid', '2019-01-01 00:00:00', 1, 90, 45, 90);
/*!40000 ALTER TABLE `rencontre` ENABLE KEYS */;

-- Listage de la structure de la table match. rencontre_arbitre
DROP TABLE IF EXISTS `rencontre_arbitre`;
CREATE TABLE IF NOT EXISTS `rencontre_arbitre` (
  `rencontre_id` int(11) NOT NULL,
  `arbitre_id` int(11) NOT NULL,
  PRIMARY KEY (`rencontre_id`,`arbitre_id`),
  KEY `IDX_1605D0C46CFC0818` (`rencontre_id`),
  KEY `IDX_1605D0C4943A5F0` (`arbitre_id`),
  CONSTRAINT `FK_1605D0C46CFC0818` FOREIGN KEY (`rencontre_id`) REFERENCES `rencontre` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1605D0C4943A5F0` FOREIGN KEY (`arbitre_id`) REFERENCES `arbitre` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.rencontre_arbitre : ~0 rows (environ)
/*!40000 ALTER TABLE `rencontre_arbitre` DISABLE KEYS */;
/*!40000 ALTER TABLE `rencontre_arbitre` ENABLE KEYS */;

-- Listage de la structure de la table match. sanction
DROP TABLE IF EXISTS `sanction`;
CREATE TABLE IF NOT EXISTS `sanction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table match.sanction : ~3 rows (environ)
/*!40000 ALTER TABLE `sanction` DISABLE KEYS */;
REPLACE INTO `sanction` (`id`, `nom`) VALUES
	(1, 'aucune'),
	(2, 'penalty'),
	(3, 'coup franc');
/*!40000 ALTER TABLE `sanction` ENABLE KEYS */;

-- Listage de la structure de la procédure match. temps_match_joueur
DROP PROCEDURE IF EXISTS `temps_match_joueur`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `temps_match_joueur`(IN match_id INT, IN joueur_id INT)
BEGIN
	DECLARE s_entree, s_sortie, s_faute INT;

	SELECT SUM(temps) INTO s_entree
		FROM changement
		WHERE entree_joueur_id = joueur_id
		AND rencontre_id = match_id;


	SELECT SUM(temps) INTO s_sortie
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND rencontre_id = match_id;

	SELECT (temps + temps_de_sortie) INTO s_faute
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND temps_de_sortie <> 0
		AND rencontre_id = match_id;
		
	SELECT if(
		(
		SELECT COUNT(jr.id) 
		FROM joueur_rencontre jr 
		WHERE (
			jr.rencontre_id = match_id AND
			jr.joueur_id = joueur_id
			) 
		)>0
	,(
	 IF(
		(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute))) 
		 IS NULL, 90
			 ,(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute))))),0) as temps_sur_terrain;

END//
DELIMITER ;

-- Listage de la structure de la procédure match. temps_moyen_joueur
DROP PROCEDURE IF EXISTS `temps_moyen_joueur`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `temps_moyen_joueur`(IN joueur_id INT)
BEGIN

	DECLARE s_entree, s_sortie, s_faute, s_change INT;
	
	SELECT SUM(temps) INTO s_entree
		FROM changement
		WHERE entree_joueur_id = joueur_id;

	SELECT SUM(temps) INTO s_sortie
		FROM changement
		WHERE sortie_joueur_id = joueur_id;

	SELECT (temps + temps_de_sortie) INTO s_faute
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND temps_de_sortie <> 0;
	
	SELECT ((s_sortie - (IF(s_entree IS NULL,0,s_entree) +IF(s_faute IS NULL,0,s_faute))) + 90)
			 /(
			 SELECT DISTINCT COUNT(r.id)
			 FROM rencontre r
			 )
		as temps_sur_terrain;		

END//
DELIMITER ;

-- Listage de la structure de la procédure match. temps_total_joueur
DROP PROCEDURE IF EXISTS `temps_total_joueur`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `temps_total_joueur`(IN joueur_id INT)
BEGIN

	DECLARE s_entree, s_sortie, s_faute, s_change INT;
	
	SELECT COUNT(temps) INTO s_change
		FROM changement 
		WHERE (entree_joueur_id =joueur_id
		OR sortie_joueur_id = joueur_id);
	
	SELECT SUM(temps) INTO s_entree
		FROM changement
		WHERE entree_joueur_id = joueur_id;


	SELECT SUM(temps) INTO s_sortie
		FROM changement
		WHERE sortie_joueur_id = joueur_id;

	SELECT (temps + temps_de_sortie) INTO s_faute
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND temps_de_sortie <> 0;
		
	SELECT IF(
		(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute))) 
		 IS NULL, (
		 	SELECT r.fin_match
			FROM match.rencontre r
			WHERE r.id = 1
			 )
			 ,(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute)))) as temps_sur_terrain;

END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
