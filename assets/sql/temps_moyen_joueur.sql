DROP PROCEDURE IF EXISTS temps_moyen_joueur;

DELIMITER $$
CREATE PROCEDURE temps_moyen_joueur(IN joueur_id INT)

BEGIN

	DECLARE s_entree, s_sortie, s_faute, s_change INT;
	
	SELECT count(temps) INTO s_change
		FROM changement 
		WHERE (entree_joueur_id = joueur_id
		OR sortie_joueur_id = joueur_id);
	
	SELECT SUM(temps) INTO s_entree
		FROM changement
		WHERE entree_joueur_id = joueur_id;


	SELECT SUM(temps) INTO s_sortie
		FROM changement
		WHERE sortie_joueur_id = joueur_id;

	SELECT (temps + temps_de_sortie) INTO s_faute
		FROM changement
		WHERE sortie_joueur_id = joueur_id
		AND temps_de_sortie <> 0;
		
	SELECT (
		(s_sortie - (s_entree +IF(s_faute IS NULL,0,s_faute)))
		 +(
		 	SELECT r.fin_match
			FROM match.rencontre r
			WHERE r.id = 1
			 ))
			 /(
			 SELECT DISTINCT COUNT(r.id)
			 FROM rencontre r
			 )
		as temps_sur_terrain;

END $$
DELIMITER ;
