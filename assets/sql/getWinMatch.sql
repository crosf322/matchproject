DROP PROCEDURE IF EXISTS getWinMatch;

DELIMITER $$
CREATE PROCEDURE getWinMatch()

BEGIN

SELECT r.idgetScoreMatch
	FROM rencontre r
	LEFT JOIN 	(
		SELECT r.id AS m_id, count(b.id) AS score_equipe
		FROM  rencontre r 
		INNER JOIN but b
			ON b.rencontre_id = r.id
		GROUP BY r.id
		) b
	ON b.m_id = r.id 
	LEFT JOIN 	(
		SELECT r.id AS m_id,count(ba.id) AS score_adverse
		FROM  rencontre r 
		INNER JOIN but_adverse ba
			ON ba.rencontre_id = r.id
		GROUP BY r.id
		) ba
	ON ba.m_id = r.id
	WHERE IF(b.score_equipe IS NULL,0,b.score_equipe) > IF(ba.score_adverse IS NULL,0,ba.score_adverse)
	
	;
END $$
DELIMITER ;
