DROP PROCEDURE IF EXISTS dreamTeam4;

DELIMITER $$
CREATE PROCEDURE dreamTeam4(IN post_id_1 INT, IN posit_id_1 INT,
                            IN post_id_2 INT, IN posit_id_2 INT,
                            IN post_id_3 INT, IN posit_id_3 INT,
                            IN post_id_4 INT, IN posit_id_4 INT)
BEGIN
  SELECT count(jr1.match_id) AS nb_victoire,
         jr1.joueur_id       AS joueur_1,
         jr2.joueur_id       AS joueur_2,
         jr3.joueur_id       AS joueur_3,
         jr4.joueur_id       AS joueur_4
  FROM (
         SELECT rencontre_id AS match_id, joueur_id
         FROM joueur_rencontre
         WHERE (poste_id, position_id) = (post_id_1, posit_id_1)
       ) AS jr1
  LEFT JOIN (
    SELECT rencontre_id AS match_id, joueur_id
    FROM joueur_rencontre
    WHERE (poste_id, position_id) = (post_id_2, posit_id_2)
  ) AS jr2
    ON jr1.match_id = jr2.match_id
  LEFT JOIN (
    SELECT rencontre_id AS match_id, joueur_id
    FROM joueur_rencontre
    WHERE (poste_id, position_id) = (post_id_3, posit_id_3)
  ) AS jr3
    ON jr1.match_id = jr3.match_id
  LEFT JOIN (
    SELECT rencontre_id AS match_id, joueur_id
    FROM joueur_rencontre
    WHERE (poste_id, position_id) = (post_id_4, posit_id_4)
  ) AS jr4
    ON jr1.match_id = jr4.match_id

  WHERE jr1.match_id IN (
    SELECT r.id
    FROM rencontre r
    LEFT JOIN (
      SELECT r.id AS m_id, count(b.id) AS score_equipe
      FROM rencontre r
      INNER JOIN but b ON b.rencontre_id = r.id
      GROUP BY r.id ) b
    ON b.m_id = r.id
    LEFT JOIN (
      SELECT r.id AS m_id, count(ba.id) AS score_adverse
      FROM rencontre r
      INNER JOIN but_adverse ba ON ba.rencontre_id = r.id
      GROUP BY r.id) ba
    ON ba.m_id = r.id
    WHERE IF(b.score_equipe IS NULL, 0, b.score_equipe) > IF(ba.score_adverse IS NULL, 0, ba.score_adverse)
  )
  GROUP BY joueur_1, joueur_2, joueur_3, joueur_4
  ORDER BY nb_victoire DESC
  LIMIT 1;
END $$
DELIMITER ;

