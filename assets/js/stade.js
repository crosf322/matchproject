let players = JSON.parse(t);

init();

function init() {
    players.forEach(function(player) {
        if(capitaine != null && player.name == capitaine.name) {
            player.status = 1
        } else if(suppleant != null && player.name == suppleant.name) {
            player.status = 2
        }
        setupPlayer(player)
    })
}

let modal = $('#set_player');

$('#add_new_player').click(function() {
    $('#set_player #joueur_match_position').attr('disabled', false);
    $('#set_player #joueur_match_poste').attr('disabled', false);
    $('#set_player #joueur_match_estRemplacant').prop('checked', true)
    $('#set_player #joueur_match_estRemplacant').attr('disabled', false);
    modal.modal();
});

$('.player_name').click(function(event) {
    let div = $(this);

    if(div.data('id') != null) {
        showPlayer(div);
        return;
    }

    let poste = div.data('poste');
    let position = div.data('position');
    $('#set_player #joueur_match_position').val(position);
    $('#set_player #joueur_match_position').attr('disabled', true);
    $('#set_player #joueur_match_poste').attr('disabled', true);
    $('#set_player #joueur_match_estRemplacant').prop('checked', false)
    $('#set_player #joueur_match_estRemplacant').attr('disabled', true);
    $('#set_player #joueur_match_poste').val(poste);
    modal.modal('show');

});

$('#add_player').click(function() {
    let player_id = $('#set_player #joueur_match_joueur').val();
    let numero = $('#set_player #joueur_match_numero').val();
    let position = $('#set_player #joueur_match_position').val();
    let poste = $('#set_player #joueur_match_poste').val();
    let estRemplacant = $('#set_player #joueur_match_estRemplacant').prop('checked');
    let status = $('#set_player #joueur_match_status').val();

    if(numero == "") {
        return;
    }

    modal.modal('hide');

    $.ajax({
        url: $('#set_player').data('target'),
        type: 'post',
        data: {
            id: match_id,
            player_id: player_id,
            numero: numero,
            position: position,
            poste: poste,
            estRemplacant: estRemplacant,
            status: status
        },
        success: function(data) {
            if(data.success) {
                let playerName = $('#set_player #joueur_match_joueur option:selected').html();

                let pl = {
                    estRemplacant: estRemplacant,
                    name: playerName,
                    id: player_id,
                    poste: (poste == null ? 0 : poste),
                    numero: numero,
                    position: ((position == null||position=="") ? 0 : position),
                    status: status
                }

                console.log(pl);

                setupPlayer(pl);
               
                updatePlayerList(pl);

                players.push(pl);
            }
        }
    })
})

function setupPlayer(player) {
    if(!player.estRemplacant) {
        setPlayerToStade(player, false)
    } else {
        addRemplacant(player);
    }

    if(player.status == 1) {
        capitaine = player;
        $('#capitaine').html(player.name)
    } else if(player.status == 2) {
        suppleant = player;
        $('#suppleant').html(player.name)
    }
}

function addRemplacant(player) {
    var lTag = jQuery("<div>").attr('data-id', player.id).attr('class', "remplacants")
        .appendTo("#remplacants");

    jQuery('<img/>',{
    }).appendTo(lTag).attr("src", "/assets/images/maillot.png");

    jQuery('<h3/>',{
    }).appendTo(lTag).html(player.name);

    jQuery('<h5/>',{
    }).appendTo(lTag).html(player.numero);

   /* var newDiv = document.createElement("div");
    newDiv.id = "r-" + player.name;
    newDiv.className = "remplacants";
    newDiv.dataset.id = player.id;

    var img = document.createElement("img");
    img.src = "/assets/images/maillot.png";

    var name = document.createElement("h3");
    console.log("NOM JOUEUR " + player.name);
    name.innerHTML = player.name;

    var num = document.createElement("h5");
    num.innerHTML = player.numero;

    newDiv.appendChild(img);
    newDiv.appendChild(name);
    newDiv.appendChild(num);

    $('#remplacants').append(newDiv);*/
}

function updatePlayerList(player) {
    players.forEach(function(element) {
        if(element.id == player.id) {
            setPlayerToStade(element, true)
            players.splice(players.indexOf(element), 1);
        }
    });
}

function setPlayerToStade(player, remove) {
    if(remove) {
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"'] p").html("?");
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"'] h3").html("?");
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"']").data('id', null);
    } else {
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"'] p").html(player.name);
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"'] h3").html(player.numero);
        $("div[data-poste='"+player.poste+"'][data-position='"+player.position+"']").data('id', player.id);
    }
}

$(document).on('click', '.remplacants', function() {
    let el = $(this);
    if(el.data('id') != null) {
        showPlayer(el)
    }
})

$('#remove_player').click(function() {
    let player_id = $('#show_player #joueur_match_joueur').val();
    let numero = $('#show_player #joueur_match_numero').val();
    let position = $('#show_player #joueur_match_position').val();
    let poste = $('#show_player #joueur_match_poste').val();
    let estRemplacant = $('#show_player #joueur_match_estRemplacant').prop('checked');
    let status = $('#show_player #joueur_match_status').val();
    let playerName = $('#show_player #joueur_match_joueur option:selected').html();

    let pl = {
        estRemplacant: estRemplacant,
        name: playerName,
        id: player_id,
        poste: poste,
        numero: numero,
        position: ((position == null||position=="") ? 0 : position),
        status: status
    }

    $('#show_player').modal('hide');

    $.ajax({
        url: $('#show_player').data('remove'),
        type: 'get',
        data: {
            id: player_id,
            match: match_id
        },
        success: function(data) {
            if(data.success) {
                removePlayer(pl);
            }
        }
    })
})

function removePlayer(pl) {
    console.log("REMOVE PLAYER");
    if(pl.estRemplacant) {
        console.log("Remplacant");
        let str = "#remplacants div[data-id='"+pl.id+"']";
        $(str).remove();
    } else {
        console.log("Pas remplacant");
        setPlayerToStade(pl, true)
    }
    
    if(pl.status == 1) {
        capitaine = null;
        $('#capitaine').html('?')
    } else if(pl.status == 2) {
        $('#suppleant').html('?')
        suppleant = null;
    }
}

function showPlayer(el) {

    $.ajax({
        url: $("#show_player").data('showurl'),
        type: 'get',
        data: {
            id: el.data('id'),
            match: match_id
        },
        success: function(data) {
            if(data.success) {
                console.log("DAT RETUNRED");
                console.log(data);
                $('#show_player #joueur_match_joueur').val(data.id);
                $('#show_player #joueur_match_numero').val(data.numero);
                $('#show_player #joueur_match_position').val(data.position);
                $('#show_player #joueur_match_poste').val(data.poste)
                console.log(data.estRemplacant);
                $('#show_player #joueur_match_estRemplacant').prop('checked', data.estRemplacant);
                $('#show_player #joueur_match_status').val(data.status);
                console.log("SHOW ");
                $('#show_player').modal();
            }
        }
    })
}